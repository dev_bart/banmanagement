package me.spacemc.banmanagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class Commands implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(label.equalsIgnoreCase("ban")){
			if(!sender.hasPermission("SpaceBans.Commands.Ban")){
				sender.sendMessage(ChatColor.RED + "You aren't allowed to use this command!");
				return true;
			}
			if(!(sender instanceof Player)){
				sender.sendMessage(ChatColor.RED + "You can't use this command!");
				return true;
			}
			
			Player p =  (Player) sender;
			
			if(args.length == 0){
				p.sendMessage(ChatColor.RED + "Use /ban <PlayerName> <Days> <Reason>");
				return true;
			}
			
			else if(args.length == 1){
				p.sendMessage(ChatColor.RED + "Use /ban <PlayerName> <Days> <Reason>");
				return true;
			}
			
			else if(args.length == 2){
				p.sendMessage(ChatColor.RED + "Use /ban <PlayerName> <Days> <Reason>");
				return true;
			}
			
			else if(args.length > 2){
				Player banned = Bukkit.getServer().getPlayer(args[0]);
				if(banned == null){
					p.sendMessage(ChatColor.RED + "That player isn't online!");
					return true;
				}
				
				if(Main.getMySQL().getBans().contains(banned.getUniqueId())){
					p.sendMessage(ChatColor.RED + "That player is already banned!");
					return true;
				}
				
				int days = Integer.parseInt(args[1]);
				
				List<String> list = new ArrayList<String>(Arrays.asList(args));
				list.remove(args[0]);
				list.remove(args[1]);
				args = list.toArray(new String[0]);
				
				String reason = "";
				for(String str : args){
						reason = (reason + str + " ");
				}
				
				reason = Main.removeLastChar(reason);
				
				Main.addBan(banned, p, reason, days);
				
				java.util.Date dt = new java.util.Date();

				java.text.SimpleDateFormat sdf = 
				     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				
				Calendar c = Calendar.getInstance(); 
				c.setTime(dt); 
				c.add(Calendar.DATE, days);
				dt = c.getTime();
				
				String unbanTime = sdf.format(dt);
				
				banned.kickPlayer("You have been banned by " + p.getName() + ", until " + unbanTime + " cause of " + reason + "!");
				return true;
			}
		}
		
		if(label.equalsIgnoreCase("unban")){
			if(!sender.hasPermission("SpaceBans.Commands.UnBan")){
				sender.sendMessage(ChatColor.RED + "You aren't allowed to use this command!");
				return true;
			}
			if(args.length == 0){
				sender.sendMessage(ChatColor.RED + "Use /unban <PlayerName>");
				return true;
			}
			
			else if(args.length == 1){
				if(!Main.getMySQL().getBannedPlayers().contains(args[0])){
					sender.sendMessage(ChatColor.RED + "That player isn't banned!");
					return true;
				}
				
				Main.removeBan(args[0]);
				sender.sendMessage(ChatColor.GREEN + "Succesfully unbanned " + args[0] + "!");
				return true;
			}
			
			else{
				sender.sendMessage(ChatColor.RED + "Too many arguments!");
				sender.sendMessage(ChatColor.RED + "Please use /unban <" + args[0] + ">");
				return true;
			}
		}
		
		if(label.equalsIgnoreCase("getAppealCode")){
			if(!sender.hasPermission("SpaceBans.Commands.GetAppealCode")){
				sender.sendMessage(ChatColor.RED + "You aren't allowed to use this command!");
				return true;
			}
			if(args.length == 0){
				sender.sendMessage(ChatColor.RED + "You need to specify a player!");
				return true;
			}
			
			else if(args.length == 1){
				if(!Main.getMySQL().getBannedPlayers().contains(args[0])){
					sender.sendMessage(ChatColor.RED + "Use /getappealcode <PlayerName>");
					return true;
				}
				
				UUID uuid = Main.getMySQL().getUUID(args[0]);
				String appealcode = Main.getMySQL().getAppealCode(uuid);
				sender.sendMessage(ChatColor.GREEN + "The appealcode of this player is " + ChatColor.RED + appealcode + "!");
				return true;
			}
			
			else{
				sender.sendMessage(ChatColor.RED + "Too many arguments!");
				sender.sendMessage(ChatColor.RED + "Please use /getappealcode <" + args[0] + ">");
				return true;
			}
		}
		if (label.equalsIgnoreCase("kick")) {
			if (!sender.hasPermission("myban.commands.kick")) {
				sender.sendMessage(ChatColor.RED + "You aren't allowed to use this command!");				
			}
			if (args.length == 0) {
				sender.sendMessage(ChatColor.RED + "Please specify a player!");
			}
			if (args.length == 1) {
				Player target = Bukkit.getServer().getPlayer(args[0]);
				target.kickPlayer("You have been kicked by " + sender);
			}
			if (args.length > 2) {
				sender.sendMessage(ChatColor.RED + "Too many agruments!");
			}
		}
		return true;
	}
}
