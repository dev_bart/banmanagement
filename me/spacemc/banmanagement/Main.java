package me.spacemc.banmanagement;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import me.spacemc.banmanagement.MySQL.MySQL;
import me.spacemc.banmanagement.MySQL.SettingsManager;
import me.spacemc.banmanagement.listener.LoginListener;

public class Main extends JavaPlugin {
	
	private static MySQL MySQL;
	
	@Override
	public void onEnable() {
		Bukkit.getServer().getPluginManager().registerEvents(new LoginListener(), this);
		
		getCommand("ban").setExecutor(new Commands());
		getCommand("unban").setExecutor(new Commands());
		getCommand("getappealcode").setExecutor(new Commands());
		getCommand("kick").setExecutor(new Commands());
		
		Setup();
	}
	
	@Override
	public void onDisable() {
		
	}
	
	public static Plugin getPlugin(){
		return Bukkit.getServer().getPluginManager().getPlugin("SpaceBans");
	}
	
	public void Setup(){
		connectMySQL();
	}
	
	public static void addBan(Player p, Player banner, String reason, int days){
		if(MySQL.getBans().contains(p.getUniqueId())){
			return;
		}
		
		MySQL.addBan(p, reason, banner, days);
	}
	
	public static void removeBan(String name){
		UUID uuid = MySQL.getUUID(name);
		MySQL.removeBan(uuid);
	}
	
	public void connectMySQL(){
		ConfigurationSection section = SettingsManager.getConfig().<ConfigurationSection>get("MySQL");
		if(!SettingsManager.getConfig().contains("MySQL.Host")){
			SettingsManager.getConfig().createSection("MySQL");
			SettingsManager.getConfig().save();
			
			ConfigurationSection s = SettingsManager.getConfig().<ConfigurationSection>get("MySQL");
			s.createSection("Host");
			s.createSection("User");
			s.createSection("Password");
			s.createSection("Database");
			s.createSection("Table");
			SettingsManager.getConfig().save();
			return;
		}
		
		if(section.getString("Host") == null || section.getString("User") == null || section.getString("Password") == null || section.getString("Database") == null){
			return;
		}
		
		String ip = section.getString("Host");
		String username = section.getString("User");
		String password = section.getString("Password");
		String db = section.getString("Database");
		
		MySQL = new MySQL(ip, username, password, db);
		
		MySQL.createTable();
		
		System.out.println("Succesfully connected to mysql database!");
	}
	
	public static MySQL getMySQL(){
		return MySQL;
	}
	
	public static int getRandomInt(int min, int max){
		Random random = new Random();
		int randomNum = random.nextInt((max - min) + 1) + min;
		return randomNum;
	}
	
	public static String getRandomString(){
		String randomString = "";
		String alphabet = "abcdefghijklmnopqrstuvwxyz";
		String numbers = "0123456789";
		
		for(int i = 0; i < 4; i++){
			int randomletter = getRandomInt(0, 25);
			int randomnumber = getRandomInt(0, 9);
			
			char number = numbers.charAt(randomnumber);
			char letter = alphabet.charAt(randomletter);
			randomString = randomString + letter + number;
		}
		return randomString;
	}
	
	public static String getNewAppealCode(){
		String appealcode = Main.getRandomString();
		
		for(UUID player : MySQL.getBans()){
			if(MySQL.getAppealCode(player) == appealcode){
				return getNewAppealCode();
			}
		}
		
		return appealcode;
	}
	
	public static String removeLastChar(String str) {
	    if (str.length() > 0 && str.charAt(str.length()-1)==' ') {
	      str = str.substring(0, str.length()-1);
	    }
	    return str;
	}
}
