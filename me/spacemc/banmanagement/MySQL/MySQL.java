package me.spacemc.banmanagement.MySQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import org.bukkit.entity.Player;

import me.spacemc.banmanagement.Main;

public class MySQL {

private Connection connection;

	private String table = SettingsManager.getConfig().<String>get("MySQL.Table");

	public MySQL(String ip, String userName, String password, String db) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + ip + "/" + db + "?user=" + userName + "&password=" + password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	public ArrayList<UUID> getBans(){
		ArrayList<UUID> players = new ArrayList<UUID>();
		try {
            PreparedStatement statement = connection.prepareStatement("select UUID from " + table + ";");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	players.add(UUID.fromString(result.getString("UUID")));
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return players;
	}
	
	public ArrayList<String> getBannedPlayers(){
		ArrayList<String> players = new ArrayList<String>();
		try {
            PreparedStatement statement = connection.prepareStatement("select UserName from " + table + ";");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	players.add(result.getString("UserName"));
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return players;
	}
	
	public String getReason(UUID uuid){
		try {
			
            PreparedStatement statement = connection.prepareStatement("select Reason from " + table + " where UUID='" + uuid.toString() + "';");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	return result.getString("Reason");
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return null;
	}
	
	public String getAppealCode(UUID uuid){
		try {
            PreparedStatement statement = connection.prepareStatement("select AppealCode from " + table + " where UUID='" + uuid.toString() + "';");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	return result.getString("AppealCode");
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return null;
	}
	
	public String getBanner(UUID uuid){
		try {
            PreparedStatement statement = connection.prepareStatement("select Banner from " + table + " where UUID='" + uuid.toString() + "';");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	return result.getString("Banner");
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return null;
	}
	
	public UUID getUUID(String name){
		try {
            PreparedStatement statement = connection.prepareStatement("select UUID from " + table + " where UserName='" + name + "';");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	return UUID.fromString(result.getString("UUID"));
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return null;
	}
	
	public java.util.Date getUnBanDate(UUID uuid){
		try {
            PreparedStatement statement = connection.prepareStatement("select UnBanDate from " + table + " where UUID='" + uuid.toString() + "';");
            ResultSet result = statement.executeQuery();
           
            while(result.next()){
            	return result.getTimestamp("UnBanDate");
            }
		} catch (Exception e) {
            e.printStackTrace();
            return null;
		}
		return null;
	}
	
	public void createTable(){
		try {
            PreparedStatement statement = connection.prepareStatement("create table if not exists " + table + " (UUID varchar(40), UserName varchar(16), Reason varchar(50),AppealCode varchar(8), Banner varchar(16), BannerUUID varchar(40), BanDate datetime, UnBanDate datetime);");
            statement.executeUpdate();
            statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addBan(Player p, String reason, Player banner, int days){
		java.util.Date dt = new java.util.Date();

		java.text.SimpleDateFormat sdf = 
		     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		String currentTime = sdf.format(dt);
		
		Calendar c = Calendar.getInstance(); 
		c.setTime(dt); 
		c.add(Calendar.DATE, days);
		dt = c.getTime();

		String UnBanTime = sdf.format(dt);
		try {
            PreparedStatement statement = connection.prepareStatement("insert into " + table + " (UUID, UserName, Reason, AppealCode, Banner, BannerUUID, BanDate, UnBanDate) values ('" + p.getUniqueId().toString() +  "', '" + p.getName() + "', '" + reason + "', '" + Main.getNewAppealCode() + "', '" + banner.getName() + "', '" + banner.getUniqueId().toString() + "', '" + currentTime + "', '" + UnBanTime + "');");
            statement.executeUpdate();
            statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void removeBan(UUID uuid){
		try {
            PreparedStatement statement = connection.prepareStatement("delete from " + table + " where UUID='" + uuid.toString() + "';");
            statement.executeUpdate();
            statement.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
