package me.spacemc.banmanagement.listener;

import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;

import me.spacemc.banmanagement.Main;

public class LoginListener implements Listener {
	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		Player p = e.getPlayer();
		
		if(Main.getMySQL() != null){
			if(Main.getMySQL().getBans().contains(p.getUniqueId())){
				Date dt = new Date();
				Date unban = Main.getMySQL().getUnBanDate(p.getUniqueId());
				if(dt.after(unban)){
					Main.removeBan(p.getName());
					return;
				}
				
				java.text.SimpleDateFormat sdf = 
					     new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				String unbanTime = sdf.format(unban);
				
				String reason = Main.getMySQL().getReason(p.getUniqueId());
				String banner = Main.getMySQL().getBanner(p.getUniqueId());
				e.disallow(null, "You have been banned by " + banner + " until " + unbanTime + " cause of " + reason + "!");
			}
		}
	}
}
